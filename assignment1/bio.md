Name: Joseph Barlan  
Hobbies: I like drawing  
Interest: I :heart: :coffee: & :art:, video games, and movies  
  
###Programming Background  
####Languages
* Python (Beginner/Intermediate)  
* C/C++ (Beginner)  
* HTML/CSS (Beginner, Self-Taught)  
* Java (Beginner)  
  
####Work Examples
* [r/OSUOnlineCS Theme](http://www.reddit.com/r/OSUOnlineCS/) [CSS]   
* [Personal Site](jbarworks.com) [Wordpress/CSS, edited Casper]  

